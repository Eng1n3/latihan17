const fs = require("fs");
const validator = require("validator");

const dirPath = "./data";
if (!fs.existsSync(dirPath)) fs.mkdirSync(dirPath);

const database = "./data/db.json";
if (!fs.existsSync(database)) fs.writeFileSync(database, "[]", "utf8");

const loadContact = () => {
	const fileDummy = fs.readFileSync(database, "utf8");
	const contacts = JSON.parse(fileDummy);
	return contacts
}

const simpanData = (nama, email, nomor) => {
	const slug = nama.toLowerCase().replace(" ", "-");
	const contact = { nama, email, nomor, slug };
	const contacts = loadContact();
	if (!validator.isEmail(email)) {
		console.log("email tidak valid");
		return false;
	};
	const duplikatEmail = contacts.find(contact => contact.email === email);
	if (duplikatEmail) {
		console.log("email telah digunakan");
		return false;
	};
	if (!validator.isMobilePhone(nomor, "id-ID")) {
		console.log("Nomor tidak valid");
		return false;
	};
	contacts.push(contact);
	fs.writeFileSync(database, JSON.stringify(contacts), "utf8");
}

const hapusData = (email) => {
	const contacts = loadContact();
	const dataContact = contacts.filter(contact => contact.email !== email );
	if (contacts.length === dataContact.length ) return false;
	fs.writeFileSync(database, JSON.stringify(dataContact), "utf8");
}

const detailData = (slug) => {
	const contacts = loadContact();
	const detailContact = contacts.find(contact => contact.slug === slug);
	return detailContact
}

module.exports = {
	loadContact,
	simpanData,
	hapusData,
	detailData
};
