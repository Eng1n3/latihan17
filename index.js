const express = require("express");
const app = express();
const expressLayouts = require("express-ejs-layouts");
const path = require("path");
const port = 3000;
const {
	loadContact,
	simpanData,
	hapusData,
	detailData,
} = require("./utils/app");

app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(path.join(__dirname + "/public")));

app.get("/", (req, res) => {
	res.render("index", {
		layout: "layouts/base",
		title: "Home"
	});
});

app.get("/about", (req, res) => {
	res.render("about", {
		layout: "layouts/base",
		title: "About",
	})
});

app.get("/contact", (req, res) => {
	const dataContact = loadContact();
	res.render("contact", {
		layout: "layouts/base",
		title: "contact",
		dataContact
	});
});

app.get("/contact/:slug", (req, res) => {
	const slug = req.params.slug;
	const contact = detailData(slug);
	res.render("detail", {
		layout: "layouts/base",
		title: "Detail Contact",
		contact
	});
});

app.use((req, res) => {
	res.status(404);
	res.send("<h1>404 Not found</h1>")
});

app.listen(port, () => {
	console.log(`Server listener at http://localhost:${port}`)
});
